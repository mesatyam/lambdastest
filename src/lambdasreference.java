interface calculator {
    void add(int num1, int num2);
}
class calc{
    static void DoAdd(int num1, int num2){
        System.out.println(num1+num2);
    }
    void letsadd(int num1, int num2){System.out.println(num1+num2);}
    calc(int num1, int num2){System.out.println("Calc constructor");}
}
public class lambdasreference {
    public static void main(String[] args){
        //1. STATIC function member reference
//        calculator cf = calc::DoAdd;
//        cf.add(3,5);

        //2. Instance function member reference
//        calc c= new calc();
//        calculator cf = c::letsadd;
//        cf.add(10,5);
        //3. Constructor reference
        calculator cf = calc::new;
        cf.add(70,9);
    }
}
