//1. not interface
//class car{
//    void name() {
//
//    }
//}
//2. Interface
interface car{
    void name();
}
//Using anonymous class defination
class ford{
    car c = new car() {
        @Override
        public void name() {
            System.out.println("FORD");
        }
    };
}
//using lambdas
class woksvagon {
    car w = ()->{System.out.println("WOKSVAGON");};
}

public class anonymousclass{
    public static void main(String[] args){
        ford f = new ford();
        f.c.name();
        woksvagon w = new woksvagon();
        w.w.name();
    }
}