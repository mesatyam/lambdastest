import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class streams {

    public static void main(String[] args) {


    List<Integer> listofnum = Arrays.asList(
            1, 90, 4, 67, 34, 78, 93, 34, 2, 1
    );
    listofnum.stream()
            .filter(i -> i!=1)
            .forEach(System.out::println);
    IntStream.range(1,10)
                .filter(i->i>=5)
                .distinct()
                .forEach(System.out::println);


}
}
