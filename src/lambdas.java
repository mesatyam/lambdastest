interface country {
    // void name(String country,int countrycode);
    // void op(innerclass i);
//     void op();
    void stat();

}

//testing what lambdas can access outside the body
public class lambdas {

    static int pin = 101;

    public static void  main(String[] args){

        //1. Parameterized member reference
//        country c = (country,countrycode) -> {
//            System.out.println(country+" has code "+ countrycode);
//            System.out.println("2 parameter Tested");
//        };
//        c.name("GO India",91);

        //2. Instance member reference
//        innerclass io = new innerclass();
//        country d = new country() {
//            @Override
//            public void op() {
//                System.out.println(io.x);
//            }
//
//        };
//        d.op();

        //3. Static member reference
        country s = () -> System.out.println(pin);
        s.stat();





    }

}
